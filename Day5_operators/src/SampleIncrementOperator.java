
public class SampleIncrementOperator {
	public static void main(String[] args) {
		int x = 25;
		System.out.println(x++); // 25
		System.out.println(x); // 26
	}
}
