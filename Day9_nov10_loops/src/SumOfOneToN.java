public class SumOfOneToN {
	public static void main(String[] args) {
		int i = 1;
		int sum = 0;
		int n = 3;
		while (i <= n) {
			sum = sum + i; // sum+=i;
			i++;
		}
		System.out.println(sum);
	}
}
