
public class Gmail {
	void login(String emailAddress, String password) {
		System.out.println("User logged in with email address " + emailAddress);
	}

	void login(long phoneNumber, String password) {
		System.out.println("User logged in with phone number " + phoneNumber);
	}

	public static void main(String[] args) {
		Gmail g = new Gmail();
		g.login(75234732745826L, "Alpha@123");
		g.login("Alpha@gmail.com", "Alpha@123");
	}
}
