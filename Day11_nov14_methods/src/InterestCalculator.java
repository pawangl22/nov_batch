
public class InterestCalculator {

	static double simpleInterest(int principal, int time, double rateOfInterest) {
		return (principal * time * rateOfInterest) / 100;
	}

	public static void main(String[] args) {
		double interest = InterestCalculator.simpleInterest(100000, 1, 5.5);
		System.out.println("The final simple interest is " + interest);
	}

}
// arrange the code properly always use -> ctrl + shift + F
// naming conventions
// 1. class name must always start with uppercase letter
// 2. variable name and method name must always start with lowercase letter