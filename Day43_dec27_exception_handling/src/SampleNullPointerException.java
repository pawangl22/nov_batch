
public class SampleNullPointerException {
	static void print(String name) {
		System.out.println(name.toUpperCase());
	}

	public static void main(String[] args) {
		print(null);
	}
}
