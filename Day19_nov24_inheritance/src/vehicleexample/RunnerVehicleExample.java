package vehicleexample;


public class RunnerVehicleExample {
	
	public static void main(String[] args) {
		Truck t = new Truck();
		System.out.println(t);
		t.color = "Red";
		t.brand = "Mercedes";
		t.price = 5000000;
		t.loadCapacity = 3000;
		System.out.println("After assigning the values");
		System.out.println(t);
		t.start();
		t.stop();
		t.dropLoad();

		System.out.println("_________________");
		Car c = new Car();
		System.out.println(c);
		c.color = "Blue";
		c.brand = "Audi";
		c.price = 3500000;
		c.isAutomatic = true;
		System.out.println(c);
		c.start();
		c.stop();
		c.cruizeControl();

		System.out.println("_________________");
		Bike b = new Bike();
		System.out.println(b);
		b.color = "Green";
		b.isKickerAvailable = false;
		b.brand = "Kawasaki";
		b.price = 1200000;
		System.out.println(b);
		b.start();
		b.stop();
		b.kick();
	}

}
