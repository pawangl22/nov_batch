
public class SampleMultiDimensionalArray {
	public static void main(String[] args) {
		int[][] arr = { { 1, 2, 3, 4, 5 }, { 8, 6, 4 } };
		for (int[] numArray : arr) {
			System.out.println("_______________");
			for (int i : numArray) {
				System.out.println(i);
			}
		}
	}
}
