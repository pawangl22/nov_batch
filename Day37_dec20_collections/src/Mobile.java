import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Mobile implements Comparable<Mobile> {

	String color;
	String brand;
	double price;

	Mobile(String color, String brand, double price) {
		this.color = color;
		this.brand = brand;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Mobile [color=" + color + ", brand=" + brand + ", price=" + price + "]\n";
	}

	@Override
	public int compareTo(Mobile o) {
		return (int) (this.price - o.price);
	}

	public static void main(String[] args) {
		Mobile m1 = new Mobile("Black", "Apple", 45000);
		Mobile m2 = new Mobile("Blue", "Samsung", 55000);
		Mobile m3 = new Mobile("White", "MI", 25000);
		Mobile m4 = new Mobile("Green", "OnePlus", 35000);

		// create a collection that holds only Mobile Objects and add all the mobiles
		ArrayList<Mobile> mobileList = new ArrayList<>();
		mobileList.add(m1);
		mobileList.add(m2);
		mobileList.add(m3);
		mobileList.add(m4);

		System.out.println(mobileList);

		Collections.sort(mobileList);
		System.out.println("Sorting phones in asceding order based on price");
		System.out.println(mobileList);

		System.out.println("Sorting based on color ascending using lambda");
		Comparator<Mobile> colorComparator = (c1, c2) -> c1.color.compareTo(c2.color);
		Collections.sort(mobileList, colorComparator.reversed());
		System.out.println(mobileList);
	}

}
