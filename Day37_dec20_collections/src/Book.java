import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Book implements Comparable<Book> {
	double price;
	String brand;
	int noOfPages;
	long sno;

	Book(double price, String brand, int noOfPages, long sno) {
		this.price = price;
		this.brand = brand;
		this.noOfPages = noOfPages;
		this.sno = sno;
	}

	@Override
	public String toString() {
		return "Book [price=" + price + ", brand=" + brand + ", noOfPages=" + noOfPages + ", sno=" + sno + "]\n";
	}

	public static void main(String[] args) {
		Book b1 = new Book(250.0, "Alpha", 200, 6565346789L);
		Book b2 = new Book(49.0, "Lion", 100, 9827356787L);
		Book b3 = new Book(195.0, "King", 150, 8162537654L);
		Book b4 = new Book(220.0, "Blue", 250, 1827364587L);

		ArrayList<Book> booksList = new ArrayList<>();
		booksList.add(b1);
		booksList.add(b2);
		booksList.add(b3);
		booksList.add(b4);

		System.out.println(booksList);

		// sort the books in brand name ascending order
		System.out.println("Sorting based on brand name ascending order");
		Collections.sort(booksList);
		System.out.println(booksList);

		System.out.println("Sorting based on price ascending order");
		PriceComparator pc = new PriceComparator();
		Collections.sort(booksList, pc);
		System.out.println(booksList);

		System.out.println("Sorting based on No. Of pages ascending order");
		PagesComparator pagesComparator = new PagesComparator();
		Collections.sort(booksList, pagesComparator);
		System.out.println(booksList);

		System.out.println("Sorting based on No. Of pages descending order");
		Collections.sort(booksList, pagesComparator.reversed());
		System.out.println(booksList);

		System.out.println("Sorting based on sno. ascending order using lambda expression");
		Comparator<Book> serialComparator = (x, y) -> (int) (x.sno - y.sno);
		Collections.sort(booksList, serialComparator);
		System.out.println(booksList);

	}

	@Override
	public int compareTo(Book o) {
		return this.brand.compareTo(o.brand);
	}
}
