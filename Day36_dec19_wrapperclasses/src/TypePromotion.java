// refer the type promotion diagram from notes to understand
// how it works?
public class TypePromotion {

	static void print(double d) {
		System.out.println("double method " + d);
	}

	static void print(long l) {
		System.out.println("long method " + l);
	}

	public static void main(String[] args) {
		char c = 'A';
		print(c);
	}
}
