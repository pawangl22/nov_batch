
public class SampleSwitchExample3 {
	public static void main(String[] args) {
		String formType = "login";
		switch (formType) {
		case "login":
			System.out.println("User logged in");
			break;

		case "logout":
			System.out.println("User logged out");
			break;

		case "profile":
			System.out.println("Profile page");
		}
	}
}
