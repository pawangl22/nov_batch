package fruitexample;

public class Apple extends Fruit {
	boolean isSeedless;

	Apple() {
		this("Red");
	}

	// ALT + SHIFT + S + C -> Generate constructor from superclass
	Apple(String color) {
		super(color);
	}

	public static void main(String[] args) {
		Apple a1 = new Apple("Red");

		Apple a2 = new Apple();
	}

}
