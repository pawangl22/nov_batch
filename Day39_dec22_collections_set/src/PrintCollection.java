import java.util.ArrayList;
import java.util.Arrays;

public class PrintCollection {
	public static void main(String[] args) {
		ArrayList<Double> heights = new ArrayList<>(Arrays.asList(1.2, 3.4, 5.6, 7.8));

		heights.forEach(e -> {
			System.out.println(e);
		});

	
		
	}
}
