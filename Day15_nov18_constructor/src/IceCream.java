
public class IceCream {
	String flavour;
	char size;
	double price;

	IceCream() {
		this("Cholate", 'R', 50);
	}

	IceCream(String flavour) {
		this.flavour = flavour;
	}

	IceCream(String flavour, char size) {
		this.flavour = flavour;
		this.size = size;
	}

	IceCream(String flavour, char size, double price) {
		this.flavour = flavour;
		this.size = size;
		this.price = price;
	}

	// override the toString() using -> alt + shift + s,s
	@Override
	public String toString() {
		return "IceCream [flavour=" + flavour + ", size=" + size + ", price=" + price + "]";
	}

	public static void main(String[] args) {
		IceCream i1 = new IceCream("Vanilla", 'L');
		System.out.println(i1);

		IceCream i2 = new IceCream();
		System.out.println(i2);
	}

}
