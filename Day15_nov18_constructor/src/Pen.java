
public class Pen {
	String color;

	// ALT + SHIFT + S + O(OWE) -> Generate Constructor using fields
	// custom 1 param constructor
	Pen(String color) {
		this.color = color;
	}

	public static void main(String[] args) {
		Pen p1 = new Pen("Blue");

		Pen p2 = new Pen("Red");

		Pen p3 = new Pen("Green");

		Pen p4 = new Pen(null);

		// Pen p5 = new Pen();
		// this will not work because there is no zero param constructor
	}

}
